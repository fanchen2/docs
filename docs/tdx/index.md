# Introduction

One of the technologies being curated and integrated by the Cent OS Virt-SIG is Intel Trust Domain Extensions (Intel TDX).
Red Hat and Intel are working together here to help bring early access to Intel TDX technology while the respective
upstream communities iterate through the review process.

The project provides RHEL-like RPM packages for the necessary components to enable TDX functionality both as a guest (VM)
and as a hypervisor (host) capable of running isolated guests on hardware with Intel TDX support.

The following sections contain documentation about using the ongoing work in the SIG to create Intel TDX protected environments.

## About Intel TDX

Intel® Trust Domain Extensions (Intel® TDX) is a hardware-backed security technology for Confidential Computing.
It introduces new, architectural elements to help deploy hardware-isolated, virtual machines (VMs) called trust domains (TDs).

Intel TDX is designed to isolate VMs from the virtual-machine manager (VMM)/hypervisor and any other non-TD software on
the platform to protect TDs from a broad range of software.
For more information see the [Intel article on TDX architecture](https://www.intel.com/content/www/us/en/developer/articles/technical/intel-trust-domain-extensions.html)
