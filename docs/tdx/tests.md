# Test Case Design

Please find BAT test cases with manual test steps on [github.com/intel/tdx/wiki/Tests](https://github.com/intel/tdx/wiki/Tests).

When running the tests on CentOS Stream, replace the "Generic" values with the "CentOS Stream / RHEL" values. See the table that follows:

| CentOS Stream / RHEL | Generic |
| ---- | ---- |
| `/usr/libexec/qemu-kvm` | `qemu-system-x86_64` |
| `/usr/share/edk2/ovmf/OVMF.inteltdx.fd` | `/usr/local/share/qemu/OVMF` |
| `/usr/libexec/qemu-bridge-helper` | `/usr/local/libexec/qemu-bridge-helper` |
